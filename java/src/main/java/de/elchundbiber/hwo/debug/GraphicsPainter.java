package de.elchundbiber.hwo.debug;

import java.awt.Color;
import java.awt.Graphics;

public class GraphicsPainter implements Painter {

  private final Graphics graphics;
  private final double margin;
  private final double scale;
  private final Color color;

  public GraphicsPainter(final Graphics graphics, final double margin, final double scale, final Color color) {
    this.graphics = graphics;
    this.margin = margin;
    this.scale = scale;
    this.color = color;
  }

  @Override
  public void drawLine(final double x0, final double y0, final double x1, final double y1) {
    graphics.setColor(color);
    graphics.drawLine((int) Math.round(margin + x0 * scale), (int) Math.round(margin + y0 * scale), (int) Math.round(margin + x1 * scale),
        (int) Math.round(margin + y1 * scale));
  }

  @Override
  public void fillCircle(final double x, final double y, final double r) {
    graphics.setColor(color);
    graphics.fillOval((int) Math.round(margin + (x - r) * scale), (int) Math.round(margin + (y - r) * scale), (int) Math.round(2 * r * scale),
        (int) Math.round(2 * r * scale));
  }

  @Override
  public void drawCircle(final double x, final double y, final double r) {
    graphics.setColor(color);
    graphics.drawOval((int) Math.round(margin + (x - r) * scale), (int) Math.round(margin + (y - r) * scale), (int) Math.round(2 * r * scale),
        (int) Math.round(2 * r * scale));
  }

}
