package de.elchundbiber.hwo.model;

import java.util.ArrayList;
import java.util.List;

public class Track {

  public String id = null;
  public String name = null;
  public List<Piece> pieces = null;
  public List<Lane> lanes = null;

  public void addPiece(final Piece piece) {
    if (pieces == null) {
      pieces = new ArrayList<>();
    }
    pieces.add(piece);
  }

  public void addLane(final Lane lane) {
    if (lanes == null) {
      lanes = new ArrayList<>();
    }
    lane.index = Integer.valueOf(lanes.size());
    lanes.add(lane);
  }

  public Lane[] lanes() {
    if (lanes == null) {
      lanes = new ArrayList<>();
    }
    return lanes.toArray(new Lane[lanes.size()]);
  }

}