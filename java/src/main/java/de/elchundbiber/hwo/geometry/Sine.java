package de.elchundbiber.hwo.geometry;

public class Sine implements Curve {

  private final double length;
  private final double spread;

  public Sine(final double length, final double spread) {
    this.length = length;
    this.spread = spread;
  }

  @Override
  public Position at(final double t) {
    final double x = t * length;
    final double y = Math.sin(Math.PI / 2.0 + t * Math.PI) * spread;
    return new Position(x, y, 0);
  }

  @Override
  public double length() {
    return new LengthApproximation(this).length();
  }

}
