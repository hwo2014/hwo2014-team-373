package de.elchundbiber.hwo.debug;

import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JFrame;

import de.elchundbiber.hwo.game.CarData;
import de.elchundbiber.hwo.graph.Vertex;
import de.elchundbiber.hwo.model.Track;

public class DebugWindow extends JFrame {

  private static final long serialVersionUID = 1L;

  private final TrackComponent trackComponent = new TrackComponent();

  public DebugWindow() {
    setTitle("Hello World Open");
    setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    setContentPane(trackComponent);
    setVisible(true);
  }

  public void update(final Track track, final Collection<CarData> cars) {
    trackComponent.setTrack(track);
    trackComponent.setCars(cars);
    pack();
  }

  public void update(final Collection<CarData> cars) {
    trackComponent.setCars(cars);
    repaint();
  }

  public void update(final LinkedList<Vertex> path) {
    trackComponent.setPath(path);
    repaint();
  }

  public void update() {
    repaint();
  }

}
