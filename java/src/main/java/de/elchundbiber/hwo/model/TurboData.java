package de.elchundbiber.hwo.model;

public class TurboData {

  public Double turboDurationMilliseconds = null;
  public Integer turboDurationTicks = null;
  public Double turboFactor = null;

}