package de.elchundbiber.hwo.model;

public class GameEnd {

  public static final String MSG_TYPE = "gameEnd";

  public static class Data {
  }

  public String msgType = null;
  public Data data = null;

}
