package de.elchundbiber.hwo.model;

public class Join extends Message {

  public static final String MSG_TYPE = "join";

  public BotId data = null;

  public static Join newInstance(final String botName, final String botKey) {
    final Join join = new Join();
    join.msgType = Join.MSG_TYPE;
    join.data = new BotId();
    join.data.name = botName;
    join.data.key = botKey;
    return join;
  }

}
