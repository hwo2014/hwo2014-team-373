package de.elchundbiber.hwo.graph;

public class Vertex {

  public final int pieceIndex;
  public final int laneIndex;

  public Vertex(final int pieceIndex, final int laneIndex) {
    this.pieceIndex = pieceIndex;
    this.laneIndex = laneIndex;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + pieceIndex;
    result = prime * result + laneIndex;
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Vertex other = (Vertex) obj;
    if (pieceIndex != other.pieceIndex) {
      return false;
    }
    if (laneIndex != other.laneIndex) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "[" + pieceIndex + ", " + laneIndex + "]";
  }

}
