package de.elchundbiber.hwo.app;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import de.elchundbiber.hwo.bot.VelocityControl;

public class Fit {

  public static void main(final String[] argv) throws Exception {

    final VelocityControl v = new VelocityControl();
    try (final BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("bumped.csv")))) {
      final int x = 0;
      String line;
      while ((line = reader.readLine()) != null) {
        final String[] data = line.split(";");
        v.observe(Integer.parseInt(data[0]), Double.parseDouble(data[1]));
      }
    }

  }

}
