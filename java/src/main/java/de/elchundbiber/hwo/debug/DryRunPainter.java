package de.elchundbiber.hwo.debug;

import java.awt.Dimension;

public class DryRunPainter implements Painter {

  public double xMin = Double.POSITIVE_INFINITY;
  public double xMax = Double.NEGATIVE_INFINITY;
  public double yMin = Double.POSITIVE_INFINITY;
  public double yMax = Double.NEGATIVE_INFINITY;

  @Override
  public void drawLine(final double x0, final double y0, final double x1, final double y1) {
    touchPoint(x0, y0);
    touchPoint(x1, y1);
  }

  @Override
  public void drawCircle(final double x, final double y, final double r) {
    touchPoint(x, y);
  }

  @Override
  public void fillCircle(final double x, final double y, final double r) {
    touchPoint(x, y);
  }

  private void touchPoint(final double x, final double y) {
    if (x < xMin) {
      xMin = x;
    }
    if (x > xMax) {
      xMax = x;
    }
    if (y < yMin) {
      yMin = y;
    }
    if (y > yMax) {
      yMax = y;
    }
  }

  public Dimension dimensions(final double margin, final double scale) {
    return new Dimension((int) Math.round((xMax - xMin) * scale + 2 * margin), (int) Math.round((yMax - yMin) * scale + 2 * margin));
  }

}
