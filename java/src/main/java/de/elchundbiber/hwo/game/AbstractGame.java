package de.elchundbiber.hwo.game;

import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import de.elchundbiber.hwo.model.BotId;
import de.elchundbiber.hwo.model.CarId;
import de.elchundbiber.hwo.model.CarPosition;
import de.elchundbiber.hwo.model.CarPositions;
import de.elchundbiber.hwo.model.Crash;
import de.elchundbiber.hwo.model.TurboData;
import de.elchundbiber.hwo.model.Finish;
import de.elchundbiber.hwo.model.GameEnd;
import de.elchundbiber.hwo.model.GameInit;
import de.elchundbiber.hwo.model.GameStart;
import de.elchundbiber.hwo.model.Join;
import de.elchundbiber.hwo.model.JoinRace;
import de.elchundbiber.hwo.model.LapFinished;
import de.elchundbiber.hwo.model.Message;
import de.elchundbiber.hwo.model.Ping;
import de.elchundbiber.hwo.model.Race;
import de.elchundbiber.hwo.model.Spawn;
import de.elchundbiber.hwo.model.Throttle;
import de.elchundbiber.hwo.model.TournamentEnd;
import de.elchundbiber.hwo.model.Turbo;
import de.elchundbiber.hwo.model.TurboAvailable;
import de.elchundbiber.hwo.model.YourCar;

public abstract class AbstractGame {

  private static final Logger LOG = Logger.getLogger(AbstractGame.class);

  private final JsonParser parser;
  {
    parser = new JsonParser();
  }

  private final Gson gson;
  {
    final GsonBuilder builder = new GsonBuilder();
    gson = builder.create();
  }

  private final TransportFactory transportFactory;

  private Transport transport = null;

  public AbstractGame(final TransportFactory transportFactory) {
    this.transportFactory = transportFactory;
  }

  public void run() throws Exception {
    transport = transportFactory.newInstance();
    try {
      onConnect();
      String line;
      while ((line = transport.readLine()) != null) {
        onReceive(line);
      }
      LOG.info("Connection closed.");
    }
    catch (final Exception e) {
      LOG.error("Game loop ended with error", e);
    }
    finally {
      transport.close();
      transport = null;
      onDisconnect();
    }
  }

  private String previousRecvMsgType = null;
  private String previousSendMsgType = null;

  private Integer currentGameTick = null;

  public final void onReceive(final String line) throws Exception {
    final JsonElement element = parser.parse(line);
    final String msgType = element.getAsJsonObject().get("msgType").getAsString();
    if (previousRecvMsgType == null || !previousRecvMsgType.equals(msgType)) {
      LOG.debug("Received: " + line);
      previousRecvMsgType = msgType;
    }
    final boolean messageHasGameTick;
    {
      final JsonElement gameTickJson = element.getAsJsonObject().get("gameTick");
      if (gameTickJson != null) {
        currentGameTick = Integer.valueOf(gameTickJson.getAsInt());
      }
      else {
        currentGameTick = null;
      }
      messageHasGameTick = currentGameTick != null;
    }
    try {
      onReceive(msgType, element);
    }
    finally {
      if (messageHasGameTick && currentGameTick != null) {
        LOG.warn(String.format("Did not properly respond to tick, sending ping as emergency fallback; currentGameTick = %s, line = %s",
            currentGameTick, line));
        send(ping());
      }
    }
  }

  private void onReceive(final String msgType, final JsonElement element) throws Exception {
    if (Join.MSG_TYPE.equals(msgType)) {
      final Join join = gson.fromJson(element, Join.class);
      onJoinRace(join.data);
    }
    else if (JoinRace.MSG_TYPE.equals(msgType)) {
      final JoinRace joinRace = gson.fromJson(element, JoinRace.class);
      onJoinRace(joinRace.data.botId);
    }
    else if (YourCar.MSG_TYPE.equals(msgType)) {
      final YourCar yourCar = gson.fromJson(element, YourCar.class);
      onYourCar(yourCar.data);
    }
    else if (GameInit.MSG_TYPE.equals(msgType)) {
      final GameInit gameInit = gson.fromJson(element, GameInit.class);
      onGameInit(gameInit.data.race);
    }
    else if (GameStart.MSG_TYPE.equals(msgType)) {
      final GameStart gameStart = gson.fromJson(element, GameStart.class);
      onGameStart(gameStart);
    }
    else if (TurboAvailable.MSG_TYPE.equals(msgType)) {
      final TurboAvailable turboAvailable = gson.fromJson(element, TurboAvailable.class);
      onTurboAvailable(turboAvailable.data);
    }
    else if (CarPositions.MSG_TYPE.equals(msgType)) {
      final CarPositions carPositions = gson.fromJson(element, CarPositions.class);
      onCarPositions(carPositions.data);
      if (currentGameTick != null) {
        onCarPositions(currentGameTick.intValue(), carPositions.data);
      }
    }
    else if (Crash.MSG_TYPE.equals(msgType)) {
      final Crash crash = gson.fromJson(element, Crash.class);
      onCrash(crash);
    }
    else if (Spawn.MSG_TYPE.equals(msgType)) {
      final Spawn tournamentEnd = gson.fromJson(element, Spawn.class);
      onSpawn(tournamentEnd);
    }
    else if (LapFinished.MSG_TYPE.equals(msgType)) {
      final LapFinished tournamentEnd = gson.fromJson(element, LapFinished.class);
      onLapFinished(tournamentEnd);
    }
    else if (Finish.MSG_TYPE.equals(msgType)) {
      final Finish finish = gson.fromJson(element, Finish.class);
      onFinish(finish.data);
    }
    else if (GameEnd.MSG_TYPE.equals(msgType)) {
      final GameEnd gameEnd = gson.fromJson(element, GameEnd.class);
      onGameEnd(gameEnd);
    }
    else if (TournamentEnd.MSG_TYPE.equals(msgType)) {
      final TournamentEnd tournamentEnd = gson.fromJson(element, TournamentEnd.class);
      onTournamentEnd(tournamentEnd);
    }
    else {
      LOG.warn("Unhandled message: " + msgType);
    }
  }

  protected abstract void onConnect() throws Exception;

  protected abstract void onJoinRace(BotId botId) throws Exception;

  protected abstract void onYourCar(CarId carId) throws Exception;

  protected abstract void onGameInit(Race race) throws Exception;

  protected abstract void onGameStart(GameStart gameStart) throws Exception;

  protected abstract void onCarPositions(List<CarPosition> carPositions) throws Exception;

  protected abstract void onCarPositions(int gameTick, List<CarPosition> carPositions) throws Exception;

  protected abstract void onTurboAvailable(TurboData turboAvailable) throws Exception;

  protected abstract void onCrash(Crash crash) throws Exception;

  protected abstract void onSpawn(Spawn spawn) throws Exception;

  protected abstract void onLapFinished(LapFinished lapFinished) throws Exception;

  protected abstract void onFinish(CarId carId);

  protected abstract void onGameEnd(GameEnd gameEnd) throws Exception;

  protected abstract void onTournamentEnd(TournamentEnd tournamentEnd) throws Exception;

  protected abstract void onDisconnect() throws Exception;

  protected Join join(final String botName, final String botKey) {
    final Join message = new Join();
    message.msgType = Join.MSG_TYPE;
    message.data = new BotId();
    message.data.name = botName;
    message.data.key = botKey;
    return message;
  }

  protected Throttle throttle(final double throttle) {
    final Throttle message = new Throttle();
    message.msgType = Throttle.MSG_TYPE;
    message.data = Double.valueOf(throttle);
    return message;
  }

  protected Turbo turbo() {
    final Turbo turbo = new Turbo();
    turbo.msgType = Turbo.MSG_TYPE;
    turbo.data = "MHAAAAW RARARARAAAA!!!";
    return turbo;
  }

  protected Ping ping() {
    final Ping message = new Ping();
    message.msgType = Ping.MSG_TYPE;
    return message;
  }

  protected void send(final Message message) throws Exception {
    if (currentGameTick != null) {
      message.gameTick = currentGameTick;
      currentGameTick = null;
    }
    final String json = gson.toJson(message);
    if (previousSendMsgType == null || !previousSendMsgType.equals(message.msgType)) {
      previousSendMsgType = message.msgType;
      LOG.debug("Send: " + json);
    }
    transport.println(json);
  }

}
