package de.elchundbiber.hwo.geometry;

public class Line implements Curve {

  private final Position origin;
  private final Position vector;
  private final double length;

  public Line(final Position at, final double distanceFromCenter, final double length) {
    this.origin = at.rotate(-Math.PI / 2.0, distanceFromCenter);
    vector = at.asOrigin().rotate(0.0, length);
    this.length = length;
  }

  @Override
  public Position at(final double t) {
    return new Position(origin.position.x + t * vector.position.x, origin.position.y + t * vector.position.y, vector.angle);
  }

  @Override
  public double length() {
    return length;
  }

}
