package de.elchundbiber.hwo.debug;

public interface Painter {

  void drawLine(double x0, double y0, double x1, double y1);

  void fillCircle(double x, double y, double r);

  void drawCircle(double x, double y, double r);

}
