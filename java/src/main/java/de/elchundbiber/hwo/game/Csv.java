package de.elchundbiber.hwo.game;

public class Csv {

  public static String join(final Object... values) {
    final StringBuilder line = new StringBuilder();
    for (int i = 0; i < values.length; ++i) {
      if (i > 0) {
        line.append(";");
      }
      line.append(values[i]);
    }
    return line.toString(); // .replace(".", ",");
  }
}
