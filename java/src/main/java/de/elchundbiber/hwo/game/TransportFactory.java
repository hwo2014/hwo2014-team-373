package de.elchundbiber.hwo.game;

public interface TransportFactory {

  Transport newInstance() throws Exception;

}
