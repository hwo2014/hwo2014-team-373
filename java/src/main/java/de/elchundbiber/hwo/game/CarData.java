package de.elchundbiber.hwo.game;

import de.elchundbiber.hwo.model.Car;
import de.elchundbiber.hwo.model.CarDimensions;
import de.elchundbiber.hwo.model.CarId;
import de.elchundbiber.hwo.model.CarPosition;
import de.elchundbiber.hwo.model.PiecePosition;

public class CarData {

  public CarId id = null;
  public CarDimensions dimensions = null;
  public Double angle = null;
  public PiecePosition piecePosition = null;
  public CarState state = null;

  public CarData(final Car car) {
    id = car.id;
    dimensions = car.dimensions;
  }

  public CarData(final CarData data) {
    id = data.id;
    dimensions = data.dimensions;
    angle = data.angle;
    piecePosition = data.piecePosition;
    state = data.state;
  }

  public void update(final CarPosition data) {
    angle = data.angle;
    piecePosition = data.piecePosition;
    if (state != CarState.CRASHED) {
      state = CarState.NORMAL;
    }
  }

  public void crash() {
    state = CarState.CRASHED;
  }

  public void spawn() {
    state = CarState.SPAWNED;
  }

  public boolean isVisible() {
    return angle != null && piecePosition != null && piecePosition.lane != null;
  }

}
