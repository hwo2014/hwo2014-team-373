package de.elchundbiber.hwo.model;

public class Switch extends Message {

  public static final String MSG_TYPE = "switchLane";

  public String data = null;

  public static Switch left() {
    return newInstance("Left");
  }

  public static Switch right() {
    return newInstance("Right");
  }

  private static Switch newInstance(final String direction) {
    final Switch message = new Switch();
    message.msgType = Switch.MSG_TYPE;
    message.data = direction;
    return message;
  }

  @Override
  public String toString() {
    return "[> " + data.toLowerCase() + "]";
  }

}
