package de.elchundbiber.hwo.model;


public class GameInit {

  public static final String MSG_TYPE = "gameInit";

  public static class Data {

    public Race race = null;

  }

  public String msgType = null;
  public Data data = null;

}
