package de.elchundbiber.hwo.game;

import java.io.Closeable;

public interface Transport extends Closeable {

  String readLine() throws Exception;

  void println(String line) throws Exception;

}
