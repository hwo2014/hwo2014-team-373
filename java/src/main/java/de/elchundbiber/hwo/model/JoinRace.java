package de.elchundbiber.hwo.model;

public class JoinRace extends Message {

  public static final String MSG_TYPE = "joinRace";

  public static class Data {

    public BotId botId = null;
    public String trackName = null;
    public Integer carCount = null;

  }

  public Data data = null;

  public static JoinRace finland(final String botName, final String botKey, final int carCount) {
    return joinRace(botName, botKey, "keimola", carCount);
  }

  public static JoinRace germany(final String botName, final String botKey, final int carCount) {
    return joinRace(botName, botKey, "germany", carCount);
  }

  public static JoinRace usa(final String botName, final String botKey, final int carCount) {
    return joinRace(botName, botKey, "usa", carCount);
  }

  public static JoinRace france(final String botName, final String botKey, final int carCount) {
    return joinRace(botName, botKey, "france", carCount);
  }

  private static JoinRace joinRace(final String botName, final String botKey, final String trackName, final int carCount) {
    final JoinRace joinRace = new JoinRace();
    joinRace.msgType = JoinRace.MSG_TYPE;
    joinRace.data = new JoinRace.Data();
    joinRace.data.botId = new BotId();
    joinRace.data.botId.name = botName;
    joinRace.data.botId.key = botKey;
    joinRace.data.trackName = trackName;
    joinRace.data.carCount = Integer.valueOf(carCount);
    return joinRace;
  }

}
