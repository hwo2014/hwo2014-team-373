package de.elchundbiber.hwo.geometry;

public class Position {

  public Point position;
  public double angle;

  public static Position zero() {
    return new Position(0.0, 0.0, Math.PI);
  }

  public Position(final double x, final double y, final double angle) {
    position = new Point(x, y);
    this.angle = angle;
  }

  public Position asOrigin() {
    return new Position(0.0, 0.0, angle);
  }

  public Position rotate(final double relativeAngle, final double distance) {
    return new Position(position.x + Math.sin(angle + relativeAngle) * distance, position.y + Math.cos(angle + relativeAngle) * distance, angle
        + relativeAngle);
  }

}
