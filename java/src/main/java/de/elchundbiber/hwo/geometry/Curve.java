package de.elchundbiber.hwo.geometry;

public interface Curve {

  Position at(double t);

  double length();

}
