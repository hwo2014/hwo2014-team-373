package de.elchundbiber.hwo.geometry;

public class Interpolation implements Curve {

  private final Curve a;
  private final Curve b;

  public Interpolation(final Curve a, final Curve b) {
    this.a = a;
    this.b = b;
  }

  @Override
  public Position at(final double t) {
    final Position pa = a.at(t);
    final Position pb = b.at(t);
    final double s = Math.cos(t * Math.PI + Math.PI) / 2.0 + 0.5;
    final double ix = pa.position.x * (1.0 - s) + pb.position.x * s;
    final double iy = pa.position.y * (1.0 - s) + pb.position.y * s;
    final double iangle = pa.angle * (1.0 - s) + pb.angle * s;
    return new Position(ix, iy, iangle);
  }

  @Override
  public double length() {
    return (a.length() + b.length()) / 2.0;
  }

}
