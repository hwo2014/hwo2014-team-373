package de.elchundbiber.hwo.geometry;

public class Cubic implements Curve {

  private final Point p0, p1, p2, p3;

  public Cubic(final Position at, final double length, final double fromDistance, final double toDistance) {
    p0 = at.rotate(-Math.PI / 2.0, fromDistance).rotate(Math.PI / 2.0, 0.0).position;
    p1 = at.rotate(-Math.PI / 2.0, fromDistance).rotate(Math.PI / 2.0, length / 2.0).position;
    p2 = at.rotate(-Math.PI / 2.0, toDistance).rotate(Math.PI / 2.0, length / 2.0).position;
    p3 = at.rotate(-Math.PI / 2.0, toDistance).rotate(Math.PI / 2.0, length).position;
  }

  @Override
  public Position at(final double t) {

    final double t0 = Math.pow(1 - t, 3.0);
    final double t1 = 3.0 * Math.pow(1 - t, 2.0) * t;
    final double t2 = 3.0 * (1 - t) * Math.pow(t, 2.0);
    final double t3 = Math.pow(t, 3.0);

    final double x = t0 * p0.x + t1 * p1.x + t2 * p2.x + t3 * p3.x;
    final double y = t0 * p0.y + t1 * p1.y + t2 * p2.y + t3 * p3.y;

    final double dt0 = -3.0 * Math.pow(1 - t, 2.0);
    final double dt1 = 3.0 * (Math.pow(1 - t, 2.0) - 2.0 * t * (1.0 - t));
    final double dt2 = 3.0 * (-Math.pow(t, 2.0) + (1.0 - t) * 2.0 * t);
    final double dt3 = 3.0 * Math.pow(t, 2.0);

    final double dx = dt0 * p0.x + dt1 * p1.x + dt2 * p2.x + dt3 * p3.x;
    final double dy = dt0 * p0.y + dt1 * p1.y + dt2 * p2.y + dt3 * p3.y;

    return new Position(x, y, Math.PI / 2.0 - Math.atan2(dy, dx));

  }

  @Override
  public double length() {
    return new LengthApproximation(this).length();
  }

}
