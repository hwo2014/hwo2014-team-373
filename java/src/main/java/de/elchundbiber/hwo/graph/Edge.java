package de.elchundbiber.hwo.graph;

import de.elchundbiber.hwo.model.Switch;

public class Edge {

  private final Vertex from;
  private final Vertex to;
  private final double distance;
  private final Switch message;

  public Edge(final Vertex from, final Vertex to, final double distance, final Switch message) {
    this.from = from;
    this.to = to;
    this.distance = distance;
    this.message = message;
  }

  public Vertex getFrom() {
    return from;
  }

  public Vertex getTo() {
    return to;
  }

  public double getDistance() {
    return distance;
  }

  public Switch getMessage() {
    return message;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((from == null) ? 0 : from.hashCode());
    result = prime * result + ((to == null) ? 0 : to.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Edge other = (Edge) obj;
    if (from == null) {
      if (other.from != null) {
        return false;
      }
    }
    else if (!from.equals(other.from)) {
      return false;
    }
    if (to == null) {
      if (other.to != null) {
        return false;
      }
    }
    else if (!to.equals(other.to)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "{" + from + " -> " + to + "}";
  }

}
