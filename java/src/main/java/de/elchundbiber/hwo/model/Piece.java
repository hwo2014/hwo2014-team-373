package de.elchundbiber.hwo.model;

import com.google.gson.annotations.SerializedName;

import de.elchundbiber.hwo.geometry.Bend;
import de.elchundbiber.hwo.geometry.Cubic;
import de.elchundbiber.hwo.geometry.Curve;
import de.elchundbiber.hwo.geometry.Line;
import de.elchundbiber.hwo.geometry.Position;
import de.elchundbiber.hwo.geometry.Quadratic;

public class Piece {

  public Double length = null;
  public Double radius = null;
  public Double angle = null;
  @SerializedName("switch")
  public Boolean isSwitch = null;

  public boolean isStraight() {
    return length != null;
  }

  public boolean isBend() {
    return radius != null && angle != null;
  }

  public boolean isSwitch() {
    return isSwitch != null && isSwitch.booleanValue();
  }

  //
  // public Curve curve(final Position at, final Lane lane) {
  // if (isStraight()) {
  // return new Line(at, lane.distanceFromCenter.doubleValue(), length.doubleValue());
  // }
  // else if (isBend()) {
  // return new Bend(at, lane.distanceFromCenter.doubleValue(), radius.doubleValue(), angle.doubleValue() / 180.0 *
  // Math.PI);
  // }
  // else {
  // throw new IllegalStateException();
  // }
  // }

  public Curve curve(final Position at, final Lane from, final Lane to) {
    if ((from.index == null && to.index == null) || ((from.index != null && to.index != null) && (from.index.intValue() == to.index.intValue()))) {
      if (isStraight()) {
        return new Line(at, from.distanceFromCenter.doubleValue(), length.doubleValue());
      }
      else if (isBend()) {
        return new Bend(at, from.distanceFromCenter.doubleValue(), radius.doubleValue(), angle.doubleValue() / 180.0 * Math.PI);
      }
    }
    else {
      if (isStraight()) {
        return new Cubic(at, length.doubleValue(), from.distanceFromCenter.doubleValue(), to.distanceFromCenter.doubleValue());
      }
      else if (isBend()) {
        return new Quadratic(at, radius.doubleValue(), angle.doubleValue() / 180.0 * Math.PI, from.distanceFromCenter.doubleValue(),
            to.distanceFromCenter.doubleValue());
      }
    }
    throw new IllegalStateException();
  }
}
