package de.elchundbiber.hwo.model;

public class CarPosition {

  public CarId id = null;
  public Double angle = null;
  public PiecePosition piecePosition = null;
  public Integer lap = null;

}