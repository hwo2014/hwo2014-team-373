package de.elchundbiber.hwo.geometry;

public class Point {

  public static Point zero() {
    return new Point(0.0, 0.0);
  }

  public final double x;
  public final double y;

  public Point(final double x, final double y) {
    this.x = x;
    this.y = y;
  }

}
