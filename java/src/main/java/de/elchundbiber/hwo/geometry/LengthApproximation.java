package de.elchundbiber.hwo.geometry;

public class LengthApproximation {

  private final int steps = 50;

  private final Curve curve;

  public LengthApproximation(final Curve curve) {
    this.curve = curve;
  }

  public double length() {
    double distance = 0.0;
    Position from = curve.at(0.0);
    for (int i = 1; i <= steps; ++i) {
      final Position to = curve.at(1.0 / steps * i);
      final double dx = to.position.x - from.position.x;
      final double dy = to.position.y - from.position.y;
      distance += Math.sqrt(dx * dx + dy * dy);
      from = to;
    }
    return distance;
  }

}
