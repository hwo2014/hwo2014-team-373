package de.elchundbiber.hwo.model;

public class CarId {

  public String name = null;
  public String color = null;

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + ((color == null) ? 0 : color.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final CarId other = (CarId) obj;
    if (name == null) {
      if (other.name != null) {
        return false;
      }
    }
    else if (!name.equals(other.name)) {
      return false;
    }
    if (color == null) {
      if (other.color != null) {
        return false;
      }
    }
    else if (!color.equals(other.color)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return String.format("[%s; %s]", name, color);
  }

}
