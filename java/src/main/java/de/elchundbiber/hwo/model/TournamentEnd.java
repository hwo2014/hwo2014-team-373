package de.elchundbiber.hwo.model;

public class TournamentEnd {

  public static final String MSG_TYPE = "tournamentEnd";

  public static class Data {
  }

  public String msgType = null;
  public Data data = null;

}
