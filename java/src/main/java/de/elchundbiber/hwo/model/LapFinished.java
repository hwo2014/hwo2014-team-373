package de.elchundbiber.hwo.model;

public class LapFinished {

  public static final String MSG_TYPE = "lapFinished";

  public static class Data {
  }

  public String msgType = null;
  public Data data = null;

}
