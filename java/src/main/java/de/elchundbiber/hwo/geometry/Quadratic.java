package de.elchundbiber.hwo.geometry;

public class Quadratic implements Curve {

  private final Point p0, p1, p2;

  double angle;

  public Quadratic(final Position at, final double radius, final double angle, final double fromDistance, final double toDistance) {

    this.angle = at.angle;

    final double signum = Math.signum(angle);

    final Position center = at.rotate(-signum * Math.PI / 2.0, radius);

    p0 = center.rotate(signum * Math.PI, radius - signum * fromDistance).position;

    p1 = center.rotate(signum * Math.PI - angle / 2.0, radius - signum * (fromDistance + toDistance) / 2.0).position;

    p2 = center.rotate(signum * Math.PI - angle, radius - signum * toDistance).position;

  }

  @Override
  public Position at(final double t) {

    final double x = (p0.x - 2.0 * p1.x + p2.x) * Math.pow(t, 2.0) + (-2.0 * p0.x + 2.0 * p1.x) * t + p0.x;
    final double y = (p0.y - 2.0 * p1.y + p2.y) * Math.pow(t, 2.0) + (-2.0 * p0.y + 2.0 * p1.y) * t + p0.y;

    final double dx = (1.0 - t) * p1.x + t * p2.x - (1.0 - t) * p0.x - t * p1.x;
    final double dy = (1.0 - t) * p1.y + t * p2.y - (1.0 - t) * p0.y - t * p1.y;

    return new Position(x, y, Math.PI / 2.0 - Math.atan2(dy, dx));

  }

  @Override
  public double length() {
    return new LengthApproximation(this).length();
  }
}
