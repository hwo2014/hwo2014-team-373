package de.elchundbiber.hwo.game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import org.apache.log4j.Logger;

public class SocketTransportFactory implements TransportFactory {

  private static final Logger LOG = Logger.getLogger(SocketTransportFactory.class);

  private final String host;
  private final int port;

  public SocketTransportFactory(final String host, final int port) {
    this.host = host;
    this.port = port;
  }

  @Override
  public Transport newInstance() throws Exception {
    LOG.info(String.format("Connecting to %s:%d", host, port));
    final Socket socket = new Socket(host, port);
    LOG.info("Connection established.");
    final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
    final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
    return new SocketTransport(socket, reader, writer);
  }

  public static final class SocketTransport implements Transport {

    private static final Logger LOG = Logger.getLogger(SocketTransport.class);

    private final Socket socket;
    private final BufferedReader reader;
    private final PrintWriter writer;

    public SocketTransport(final Socket socket, final BufferedReader reader, final PrintWriter writer) {
      this.socket = socket;
      this.reader = reader;
      this.writer = writer;
    }

    @Override
    public String readLine() throws Exception {
      final long start = System.currentTimeMillis();
      final String line = reader.readLine();
      // LOG.debug(String.format("Waited %.03f ms for new message.", (System.currentTimeMillis() - start) / 1000.0));
      return line;
    }

    @Override
    public void println(final String line) throws Exception {
      writer.println(line);
      writer.flush();
    }

    @Override
    public void close() throws IOException {
      socket.close();
    }

  }

}
