package de.elchundbiber.hwo.debug;

import de.elchundbiber.hwo.geometry.Point;

public class Cursor {

  private final Painter painter;

  private double x;
  private double y;

  public Cursor(final Painter painter) {
    this.painter = painter;
  }

  public void moveTo(final Point p) {
    this.x = p.x;
    this.y = p.y;
  }

  public void lineTo(final Point p) {
    painter.drawLine(this.x, this.y, p.x, p.y);
    this.x = p.x;
    this.y = p.y;
  }

}
