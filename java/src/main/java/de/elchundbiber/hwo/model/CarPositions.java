package de.elchundbiber.hwo.model;

import java.util.List;

public class CarPositions {

  public static final String MSG_TYPE = "carPositions";

  public String msgType = null;
  public List<CarPosition> data = null;
  public Integer gameTick = null;

}
