package de.elchundbiber.hwo.geometry;

public class Bend implements Curve {

  private final double signum;
  private final double radius;
  private final Position at;
  private final double angle;

  public Bend(final Position at, final double distanceFromCenter, final double radius, final double angle) {
    this.signum = Math.signum(angle);
    this.radius = radius - signum * distanceFromCenter;
    this.at = at.rotate(-signum * Math.PI / 2.0, radius);
    this.angle = angle;
  }

  @Override
  public Position at(final double t) {
    return at.rotate(-Math.PI - t * angle, radius).rotate(-signum * Math.PI / 2.0, 0.0);
  }

  @Override
  public double length() {
    return Math.abs(angle) * radius;
  }

}
