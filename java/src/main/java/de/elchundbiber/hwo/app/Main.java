package de.elchundbiber.hwo.app;

import org.apache.log4j.Logger;

import de.elchundbiber.hwo.game.SocketTransportFactory;
import de.elchundbiber.hwo.game.WinningGame;
import de.elchundbiber.hwo.model.Join;
import de.elchundbiber.hwo.model.JoinRace;
import de.elchundbiber.hwo.model.Message;

public class Main {

  private static final Logger LOG = Logger.getLogger(Main.class);

  public static void main(final String[] argv) throws Exception {

    final boolean debug = Boolean.getBoolean("debug");

    final String host = argv[0];
    final int port = Integer.parseInt(argv[1]);
    final String botName = argv[2];
    final String botKey = argv[3];

    LOG.info(String.format("Initializing; host = %s, port = %d, botName = %s, botKey = %s", host, port, botName, botKey));

    final Message joinMessage;
    if (debug) {
      final int carCount = 1;
      final Join justJoin = Join.newInstance(botName, botKey);
      final JoinRace finland = JoinRace.finland(botName, botKey, carCount);
      final JoinRace germany = JoinRace.germany(botName, botKey, carCount);
      final JoinRace usa = JoinRace.usa(botName, botKey, carCount);
      final JoinRace france = JoinRace.france(botName, botKey, carCount);
      joinMessage = usa;
    }
    else {
      joinMessage = Join.newInstance(botName, botKey);
    }

    try {
      // new Game(new SocketTransportFactory(host, port), joinMessage, debug, 0.654).run(); // 0.85, 0.9
      new WinningGame(new SocketTransportFactory(host, port), joinMessage, debug, 3.5).run();
    }
    catch (final Exception e) {
      LOG.error("Unexpected exit", e);
    }

  }
}
