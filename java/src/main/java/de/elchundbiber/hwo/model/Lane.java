package de.elchundbiber.hwo.model;

public class Lane {

  public static final Lane CENTER = fromCenter(0.0);

  public Double distanceFromCenter = null;
  public Integer index = null;

  public static Lane fromCenter(final double distance) {
    final Lane lane = new Lane();
    lane.distanceFromCenter = Double.valueOf(distance);
    return lane;
  }

}