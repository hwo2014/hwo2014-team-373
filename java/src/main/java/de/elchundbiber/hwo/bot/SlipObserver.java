package de.elchundbiber.hwo.bot;

import org.apache.log4j.Logger;

import de.elchundbiber.hwo.geometry.Position;
import de.elchundbiber.hwo.model.Lane;
import de.elchundbiber.hwo.model.Piece;
import de.elchundbiber.hwo.model.PiecePosition;
import de.elchundbiber.hwo.model.Track;

public class SlipObserver {

  private static final Logger LOG = Logger.getLogger(SlipObserver.class);

  enum Mode {

    IDLE,

    OBSERVING,

    WAITING_FOR_CURVE,

    IN_FIRST_CURVE,

    WAITING_FOR_SECOND_CURVE,

    IN_SECOND_CURVE,

    PRE_DETHROTTLE_IN_SECOND_CURVE,

    DETHROTTLE_IN_SECOND_CURVE,

    WAITING_FOR_THIRD_CURVE,

    IN_THIRD_CURVE,

    DONE;

  }

  private Mode mode = Mode.IDLE;

  private double currentSpeed;

  private final double maxThrottle = 1.0;

  private double targetSpeedInCurve;

  private boolean slipped;

  private int stopInPiece;

  private double stopAfterPercentageInPiece;

  public void updateCurrentSpeed(final int gameTick, final double currentSpeed) {
    this.currentSpeed = currentSpeed;
  }

  public void updateSlipAngle(final double slipAngle) {
    if (mode == Mode.IN_FIRST_CURVE || mode == Mode.IN_SECOND_CURVE || mode == Mode.PRE_DETHROTTLE_IN_SECOND_CURVE
        || mode == Mode.DETHROTTLE_IN_SECOND_CURVE || mode == Mode.IN_THIRD_CURVE) {
      if (slipAngle > 0.0) {
        if (!slipped) { // ...yet
          LOG.debug("Observed slip angle");
          slipped = true;
        }
      }
    }
  }

  public void enterPiece(final Track track, final Piece piece, final PiecePosition position) {
    if (piece.isBend()) {
      if (mode == Mode.WAITING_FOR_CURVE) {
        slipped = false;
        targetSpeedInCurve = currentSpeed;
        mode = Mode.IN_FIRST_CURVE;
        LOG.debug(String.format("Entering the first curve, should maintain speed; targetSpeedInCurve = %.03f", targetSpeedInCurve));
      }
      else if (mode == Mode.WAITING_FOR_SECOND_CURVE) {
        slipped = false;
        mode = Mode.IN_SECOND_CURVE;
        final int currentPiece = position.pieceIndex.intValue();
        int endPiece = position.pieceIndex.intValue();
        while (piece.radius.equals(track.pieces.get((endPiece + 1) % track.pieces.size()).radius)) {
          endPiece++;
        }
        double totalBendLength = 0.0;
        for (int i = currentPiece; i <= endPiece; ++i) {
          totalBendLength += track.pieces.get(i).curve(Position.zero(), Lane.CENTER, Lane.CENTER).length();
        }
        double drivingDistance = 0.0;
        stopInPiece = currentPiece;
        stopAfterPercentageInPiece = 0.0;
        for (;; ++stopInPiece) {
          final double length = track.pieces.get(stopInPiece).curve(Position.zero(), Lane.CENTER, Lane.CENTER).length();
          if (drivingDistance + length < totalBendLength / 2.0) {
            drivingDistance += length;
          }
          else {
            stopAfterPercentageInPiece = ((totalBendLength / 2.0) - drivingDistance) / length;
            break;
          }
        }
        LOG.debug(String.format("Entering the second curve; currentPiece = %d, stopInPiece = %d, stopAfterPercentageInPiece = %.03f", currentPiece,
            stopInPiece, stopAfterPercentageInPiece));
      }
      if (mode == Mode.IN_SECOND_CURVE) {
        if (position.pieceIndex.intValue() == stopInPiece) {
          LOG.debug("Middle piece in second curve reached");
          mode = Mode.PRE_DETHROTTLE_IN_SECOND_CURVE;
        }
      }
      if (mode == Mode.PRE_DETHROTTLE_IN_SECOND_CURVE) { // check immediately
        final int currentPiece = position.pieceIndex.intValue();
        final int startLane = position.lane.startLaneIndex.intValue();
        final int endLane = position.lane.endLaneIndex.intValue();
        final Lane[] lanes = track.lanes();
        final double length = track.pieces.get(currentPiece).curve(Position.zero(), lanes[startLane], lanes[endLane]).length();
        if (currentPiece != stopInPiece || position.inPieceDistance.doubleValue() / length > stopAfterPercentageInPiece) {
          LOG.debug(String.format("De-throttling in second curve; currentPiece = %d, stopPiece = %d, inPieceDistance = %.03f, length = %.03f",
              currentPiece, stopInPiece, position.inPieceDistance, length));
          mode = Mode.DETHROTTLE_IN_SECOND_CURVE;
        }
      }
      if (mode == Mode.WAITING_FOR_THIRD_CURVE) {
        slipped = false;
        targetSpeedInCurve = currentSpeed;
        LOG.debug(String.format("Entering the third curve, should maintain speed; targetSpeedInCurve = %.03f", targetSpeedInCurve));
        mode = Mode.IN_THIRD_CURVE;
      }
    }
    else if (piece.isStraight()) {
      if (mode == Mode.IN_FIRST_CURVE) {
        if (slipped) {
          LOG.debug("Left bend with successful observation of slip angle, now waiting for second curve");
          mode = Mode.WAITING_FOR_SECOND_CURVE;
        }
        else {
          LOG.debug("Left the bend, but no slip angle was observed");
          mode = Mode.WAITING_FOR_CURVE;
        }
      }
      if (mode == Mode.DETHROTTLE_IN_SECOND_CURVE) {
        LOG.debug("Left second curve dethrottling, now waiting for third curve");
        mode = Mode.WAITING_FOR_THIRD_CURVE;
      }
      else if (mode == Mode.IN_THIRD_CURVE) {
        LOG.debug("Left third curve, all observations done");
        mode = Mode.DONE;
      }
      else if (mode == Mode.OBSERVING) {
        LOG.debug("Initializing, now waiting for first curve");
        mode = Mode.WAITING_FOR_CURVE;
      }
    }
  }

  public double getThrottle(final VelocityControl vControl) {
    if (mode == Mode.IN_FIRST_CURVE || mode == Mode.WAITING_FOR_SECOND_CURVE || mode == Mode.IN_SECOND_CURVE
        || mode == Mode.PRE_DETHROTTLE_IN_SECOND_CURVE || mode == Mode.IN_THIRD_CURVE) {
      // LOG.debug("Returning controlled throttle");
      return vControl.getThrottle(targetSpeedInCurve);
    }
    if (mode == Mode.DETHROTTLE_IN_SECOND_CURVE) {
      // LOG.debug("Returning break");
      return 0.0;
    }
    if (mode == Mode.WAITING_FOR_THIRD_CURVE) {
      // LOG.debug("Returning maximum throttle");
      return maxThrottle;
    }
    else {
      if (mode == Mode.IDLE) {
        // LOG.info("Observation begins");
        mode = Mode.OBSERVING;
      }
      return maxThrottle;
    }
  }
}
