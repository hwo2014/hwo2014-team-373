package de.elchundbiber.hwo.model;

public class GameStart {

  public static final String MSG_TYPE = "gameStart";

  public static class Data {
  }

  public String msgType = null;
  public Data data = null;

}
