package de.elchundbiber.hwo.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Dijkstra {

  private final List<Edge> edges;
  private Set<Vertex> settledNodes;
  private Set<Vertex> unSettledNodes;
  private Map<Vertex, Vertex> predecessors;
  private Map<Vertex, Double> distance;

  public Dijkstra(final List<Edge> edges) {
    this.edges = new ArrayList<>(edges);
  }

  public void execute(final Vertex source) {
    settledNodes = new HashSet<>();
    unSettledNodes = new HashSet<>();
    distance = new HashMap<>();
    predecessors = new HashMap<Vertex, Vertex>();
    distance.put(source, 0.0);
    unSettledNodes.add(source);
    while (unSettledNodes.size() > 0) {
      final Vertex node = getMinimum(unSettledNodes);
      settledNodes.add(node);
      unSettledNodes.remove(node);
      findMinimalDistances(node);
    }
  }

  private void findMinimalDistances(final Vertex node) {
    final List<Vertex> adjacentNodes = getNeighbors(node);
    for (final Vertex target : adjacentNodes) {
      if (getShortestDistance(target) > getShortestDistance(node) + getDistance(node, target).getDistance()) {
        distance.put(target, getShortestDistance(node) + getDistance(node, target).getDistance());
        predecessors.put(target, node);
        unSettledNodes.add(target);
      }
    }

  }

  public Edge getDistance(final Vertex node, final Vertex target) {
    for (final Edge edge : edges) {
      if (edge.getFrom().equals(node) && edge.getTo().equals(target)) {
        return edge;
      }
    }
    throw new RuntimeException("Should not happen; " + node + ", " + target);
  }

  private List<Vertex> getNeighbors(final Vertex node) {
    final List<Vertex> neighbors = new ArrayList<Vertex>();
    for (final Edge edge : edges) {
      if (edge.getFrom().equals(node) && !isSettled(edge.getTo())) {
        neighbors.add(edge.getTo());
      }
    }
    return neighbors;
  }

  private Vertex getMinimum(final Set<Vertex> vertexes) {
    Vertex minimum = null;
    for (final Vertex vertex : vertexes) {
      if (minimum == null) {
        minimum = vertex;
      }
      else {
        if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
          minimum = vertex;
        }
      }
    }
    return minimum;
  }

  private boolean isSettled(final Vertex vertex) {
    return settledNodes.contains(vertex);
  }

  private double getShortestDistance(final Vertex destination) {
    final Double d = distance.get(destination);
    if (d == null) {
      return Double.MAX_VALUE;
    }
    else {
      return d;
    }
  }

  /*
   * This method returns the path from the source to the selected target and NULL if no path exists
   */
  public LinkedList<Vertex> getPath(final Vertex target) {
    final LinkedList<Vertex> path = new LinkedList<Vertex>();
    Vertex step = target;
    // check if a path exists
    if (predecessors.get(step) == null) {
      return null;
    }
    path.add(step);
    while (predecessors.get(step) != null) {
      step = predecessors.get(step);
      path.add(step);
    }
    // Put it into the correct order
    Collections.reverse(path);
    return path;
  }

}
