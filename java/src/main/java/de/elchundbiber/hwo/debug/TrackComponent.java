package de.elchundbiber.hwo.debug;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.JComponent;

import de.elchundbiber.hwo.game.CarData;
import de.elchundbiber.hwo.geometry.Curve;
import de.elchundbiber.hwo.geometry.Position;
import de.elchundbiber.hwo.graph.Vertex;
import de.elchundbiber.hwo.model.Lane;
import de.elchundbiber.hwo.model.Piece;
import de.elchundbiber.hwo.model.Track;

public class TrackComponent extends JComponent {

  private static final long serialVersionUID = 1L;

  private final transient double margin = 32.0, scale = 0.6;

  private transient Track track = null;
  private transient double x = 0.0, y = 0.0;

  private transient Collection<CarData> cars = null;
  private transient Collection<Vertex> path = null;

  public void setTrack(final Track track) {
    final DryRunPainter dryRun = new DryRunPainter();
    {
      drawTrack(dryRun, 0.0, 0.0, track);
      setPreferredSize(dryRun.dimensions(margin, scale));
    }
    this.track = track;
    x = -dryRun.xMin;
    y = -dryRun.yMin;
  }

  public void setCars(final Collection<CarData> cars) {
    this.cars = new ArrayList<>(cars);
  }

  public void setPath(final LinkedList<Vertex> path) {
    this.path = new ArrayList<>(path);
  }

  @Override
  public void paint(final Graphics g) {
    super.paint(g);
    if (track != null) {
      final Painter painter = new GraphicsPainter(g, margin, scale, Color.BLACK);
      drawTrack(painter, x, y, track);
    }
    if (track != null && cars != null) {
      for (final CarData car : cars) {
        if (!car.isVisible()) {
          continue;
        }
        final Painter painter = new GraphicsPainter(g, margin, scale, Color.RED);
        drawCar(painter, x, y, track, car);
      }
    }
  }

  private void drawTrack(final Painter painter, final double x, final double y, final Track track) {

    Position currentPosition = startPosition();

    for (final Piece piece : track.pieces) {

      final Lane[] lanes = track.lanes();

      for (final Lane lane : lanes) {
        final Curve curve = piece.curve(currentPosition, lane, lane);
        drawCurve(painter, curve);
      }

      if (piece.isSwitch()) {
        for (int i = 0; i < lanes.length; ++i) {
          if (i - 1 >= 0) {
            final Curve curve = piece.curve(currentPosition, lanes[i], lanes[i - 1]);
            drawCurve(painter, curve);
          }
          if (i + 1 < lanes.length) {
            final Curve curve = piece.curve(currentPosition, lanes[i], lanes[i + 1]);
            drawCurve(painter, curve);
          }
        }
      }

      currentPosition = piece.curve(currentPosition, Lane.CENTER, Lane.CENTER).at(1.0);

    }

  }

  private Position startPosition() {
    return new Position(x, y, Math.PI / 2.0);
  }

  private void drawCurve(final Painter painter, final Curve curve) {
    final int steps = 20;
    final Cursor cursor = new Cursor(painter);
    cursor.moveTo(curve.at(0.0).position);
    for (int i = 1; i <= steps; ++i) {
      cursor.lineTo(curve.at(1.0 / steps * i).position);
    }
  }

  private void drawCar(final Painter painter, final double x, final double y, final Track track, final CarData car) {

    final int pieceIndex = car.piecePosition.pieceIndex.intValue();

    final int fromIndex = car.piecePosition.lane.startLaneIndex.intValue();
    final int toIndex = car.piecePosition.lane.endLaneIndex.intValue();

    Position p = startPosition();
    for (int i = 0; i <= pieceIndex; ++i) {
      if (i < pieceIndex) {
        p = track.pieces.get(i).curve(p, Lane.CENTER, Lane.CENTER).at(1.0);
      }
      else {
        final Lane from = track.lanes.get(fromIndex);
        final Lane to = track.lanes.get(toIndex);
        final Curve curve = track.pieces.get(i).curve(p, from, to);
        p = curve.at(car.piecePosition.inPieceDistance.doubleValue() / curve.length());
      }
    }

    switch (car.state) {
    case NORMAL:
      p = p.rotate(-car.angle / 180.0 * Math.PI, 0.0);
      final Position head = p.rotate(0.0, car.dimensions.guideFlagPosition.doubleValue());
      final Position right = p.rotate(Math.PI / 2.0, car.dimensions.width.doubleValue() / 2.0);
      final Position left = p.rotate(-Math.PI / 2.0, car.dimensions.width.doubleValue() / 2.0);
      final Position back = p.rotate(Math.PI, car.dimensions.length.doubleValue() - car.dimensions.guideFlagPosition.doubleValue());
      painter.drawLine(back.position.x, back.position.y, head.position.x, head.position.y);
      painter.drawLine(left.position.x, left.position.y, right.position.x, right.position.y);
      break;
    case CRASHED:
      painter.fillCircle(p.position.x, p.position.y, car.dimensions.width.doubleValue() / 2.0);
      break;
    case SPAWNED:
      painter.drawCircle(p.position.x, p.position.y, car.dimensions.width.doubleValue() / 2.0);
      break;
    }

  }

}
