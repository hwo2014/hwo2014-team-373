package de.elchundbiber.hwo.bot;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class VelocityControl {

  private static final Logger LOG = Logger.getLogger(VelocityControl.class);

  private static final class Observation {

    public final int tick;
    public final double speed;
    public Double calculatedSpeed;

    public Observation(final int tick, final double speed) {
      this.tick = tick;
      this.speed = speed;
    }

    @Override
    public String toString() {
      if (calculatedSpeed == null) {
        return String.format("[%d, %.05f]", tick, speed);
      }
      else {
        return String.format("[%d, %.05f, %.05f]", tick, speed, calculatedSpeed);
      }
    }

  }

  private final List<Observation> observations = new ArrayList<>(600);

  private final double learningThrottle = 0.01;

  private Double saturatedSpeed = null;

  private final double epsilon = 0.000001;

  private boolean initialized = false;

  private boolean learning = true;

  private boolean hasBeenBumped = false;

  private Integer tickWhenInitialized = null;

  private Double speedAtBump = null;

  private Double observedSpeed = null;

  private Double enginePower = null;

  public boolean observe(final int gameTick, final double speed) {
    if (learning && speed > 0.0) {
      final boolean isFirstEverObservation = !initialized;
      if (isFirstEverObservation) {
        tickWhenInitialized = Integer.valueOf(gameTick);
        initialized = true;
      }
      else {
        boolean carHasBeenBumped, speedIsDoneChanging;
        final double dV = speed - observedSpeed.doubleValue();
        carHasBeenBumped = dV > 0.5;
        speedIsDoneChanging = Math.abs(dV) < epsilon;
        if (carHasBeenBumped) {
          LOG.warn(String.format("Bump detected; gameTick = %d, speed = %.03f, dV = %.03f", gameTick, speed, dV));
          hasBeenBumped = true;
          speedAtBump = Double.valueOf(speed);
          observations.clear();
          tickWhenInitialized = Integer.valueOf(gameTick);
        }
        else if (speedIsDoneChanging) {
          saturatedSpeed = Double.valueOf(speed);
          computerEnginePower();
          observations.clear();
          learning = false;
          return false;
        }
      }
      observations.add(new Observation(gameTick - tickWhenInitialized.intValue(), speed));
    }
    observedSpeed = Double.valueOf(speed);
    return learning;
  }

  private void computerEnginePower() {
    LOG.debug(String.format("Estimating engine power...; observations.size = %d, observations = %s", observations.size(), observations));
    double minEnginePower = 0.001;
    double maxEnginePower = 1.0;
    double estimate, leastSquares;
    int iterations = 0;
    do {
      estimate = (minEnginePower + maxEnginePower) / 2.0;
      leastSquares = 0.0;
      for (final Observation observation : observations) {
        if (!hasBeenBumped) {
          observation.calculatedSpeed = Double.valueOf(-saturatedSpeed.doubleValue() * (1.0 / Math.exp(estimate * observation.tick) - 1));
        }
        else {
          observation.calculatedSpeed = Double.valueOf((speedAtBump.doubleValue() - saturatedSpeed.doubleValue())
              / Math.exp(estimate * observation.tick) + saturatedSpeed.doubleValue());
        }
        final double difference = observation.calculatedSpeed.doubleValue() - observation.speed;
        leastSquares += difference;
      }
      if (hasBeenBumped) {
        leastSquares *= -1;
      }
      if (leastSquares < 0) {
        minEnginePower = estimate;
      }
      else if (leastSquares > 0) {
        maxEnginePower = estimate;
      }
      else {
        break;
      }
      iterations++;
    }
    while (iterations < 100 && epsilon < Math.abs(leastSquares));
    enginePower = Double.valueOf(estimate);
    LOG.debug(String.format("Determined engine power; iterations = %d, enginePower = %.05f, observations = %s", Integer.valueOf(iterations),
        enginePower, observations));
  }

  public Double getThrottle(final double targetSpeed) {
    if (learning) {
      return Double.valueOf(learningThrottle);
    }
    else if (saturatedSpeed != null) {
      return Double.valueOf(learningThrottle / saturatedSpeed.doubleValue() * targetSpeed);
      // if (observedSpeed.doubleValue() < targetSpeed) {
      // return Double.valueOf(-eventualSpeed.doubleValue() * (1.0 / Math.exp(estimate * observation.tick) - 1));
      // return () * targetSpeed;
      // return Double.valueOf(1.0);
      // }
      // if (observedSpeed.doubleValue() > targetSpeed) {
      // return Double.valueOf(0.0);
      // }
    }
    return null;
  }
}
