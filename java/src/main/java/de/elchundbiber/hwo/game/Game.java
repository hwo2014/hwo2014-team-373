package de.elchundbiber.hwo.game;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import de.elchundbiber.hwo.bot.VelocityControl;
import de.elchundbiber.hwo.debug.DebugWindow;
import de.elchundbiber.hwo.geometry.Position;
import de.elchundbiber.hwo.graph.Dijkstra;
import de.elchundbiber.hwo.graph.Edge;
import de.elchundbiber.hwo.graph.Vertex;
import de.elchundbiber.hwo.model.BotId;
import de.elchundbiber.hwo.model.Car;
import de.elchundbiber.hwo.model.CarId;
import de.elchundbiber.hwo.model.CarPosition;
import de.elchundbiber.hwo.model.Crash;
import de.elchundbiber.hwo.model.TurboData;
import de.elchundbiber.hwo.model.GameEnd;
import de.elchundbiber.hwo.model.GameStart;
import de.elchundbiber.hwo.model.Lane;
import de.elchundbiber.hwo.model.LapFinished;
import de.elchundbiber.hwo.model.Message;
import de.elchundbiber.hwo.model.Piece;
import de.elchundbiber.hwo.model.Race;
import de.elchundbiber.hwo.model.Spawn;
import de.elchundbiber.hwo.model.Switch;
import de.elchundbiber.hwo.model.TournamentEnd;
import de.elchundbiber.hwo.model.Track;

public class Game extends AbstractGame {

  private static final Logger LOG = Logger.getLogger(Game.class);

  private final Message joinMessage;
  private final boolean debug;

  private DebugWindow debugWindow = null;
  private Track track = null;
  private Map<CarId, CarData> cars = null;
  private CarId me = null;
  private PrintWriter csv = null;
  private final double maxThrottle;

  private final VelocityControl velocity = new VelocityControl();

  public Game(final TransportFactory transportFactory, final Message joinMessage, final boolean debug, final double maxThrottle) {
    super(transportFactory);
    this.joinMessage = joinMessage;
    this.debug = debug;
    this.maxThrottle = maxThrottle;
  }

  @Override
  public void onConnect() throws Exception {
    if (debug) {
      debugWindow = new DebugWindow();
    }
    send(joinMessage);
  }

  @Override
  protected void onJoinRace(final BotId botId) {
  }

  @Override
  protected void onYourCar(final CarId carId) {
    me = carId;
  }

  @Override
  public void onGameInit(final Race race) throws Exception {
    track = race.track;
    cars = new HashMap<>();
    try {
      csv = new PrintWriter(new FileOutputStream("log.csv"));
    }
    catch (final IOException e) {
    }
    for (final Car car : race.cars) {
      cars.put(car.id, new CarData(car));
    }
    if (debug && debugWindow != null) {
      debugWindow.update(race.track, cars.values());
    }
  }

  @Override
  public void onGameStart(final GameStart gameStart) {
  }

  boolean sent = false;
  CarData previousData = null;

  Map<Integer, Switch> switches = null;
  double totalDistance = 0.0;

  double currentThrottle = 0.8;
  Double previousDistance = null;
  Double previousAngle = null;

  private boolean straight = true;

  private int bends = -1;

  private final double factor = 1.0;

  private final boolean speedUp = true;

  @Override
  public void onCarPositions(final List<CarPosition> carPositions) throws Exception {
    for (final CarPosition data : carPositions) {
      cars.get(data.id).update(data);
    }
  }

  @Override
  public void onCarPositions(final int gameTick, final List<CarPosition> carPositions) throws Exception {

    final CarData data = cars.get(me);

    if (switches == null) {

      switches = new HashMap<>();

      final Lane[] lanes = track.lanes();
      final List<Edge> edges = new ArrayList<>();
      final int current = data.piecePosition.pieceIndex.intValue();
      final int pieces = track.pieces.size();

      final int start = current;
      final int end = (current + pieces - 1) % pieces;

      for (int c = 0; c < track.pieces.size(); ++c) {

        final int i = (start + c) % pieces;
        final int k = (i + 1) % pieces;

        final Piece piece = track.pieces.get(i);

        for (int j = 0; j < lanes.length; ++j) {

          final Vertex from = new Vertex(i, j);
          {
            final Vertex to = new Vertex(k, j);
            final Edge edge = new Edge(from, to, piece.curve(Position.zero(), lanes[j], lanes[j]).length(), null);
            edges.add(edge);
          }

          if (piece.isSwitch()) {
            if (j - 1 >= 0) {
              final Vertex to = new Vertex(k, j - 1);
              final Edge edge = new Edge(from, to, piece.curve(Position.zero(), lanes[j], lanes[j - 1]).length(), Switch.left());
              edges.add(edge);
            }
            if (j + 1 < lanes.length) {
              final Vertex to = new Vertex(k, j + 1);
              final Edge edge = new Edge(from, to, piece.curve(Position.zero(), lanes[j], lanes[j + 1]).length(), Switch.right());
              edges.add(edge);
            }
          }

        }
      }

      LOG.debug(String.format("Graph; edges = %s", edges));

      double minDistance = Double.POSITIVE_INFINITY;
      for (int j = 0; j < lanes.length; ++j) {
        final Dijkstra dijkstra = new Dijkstra(edges);
        dijkstra.execute(new Vertex(current, data.piecePosition.lane.endLaneIndex.intValue()));
        final List<Vertex> path = dijkstra.getPath(new Vertex(end, j));
        final Map<Integer, Switch> sw = new TreeMap<>();
        double distance = 0.0;
        for (int i = 0; i < path.size() - 2; ++i) {
          final Vertex a = path.get(i + 1);
          final Vertex b = path.get(i + 2);
          final Edge edge = dijkstra.getDistance(a, b);
          distance += edge.getDistance();
          final Switch message = edge.getMessage();
          // if (path.get(n).laneIndex < path.get(m).laneIndex) {
          // message = Switch.left();
          // }
          // if (path.get(n).laneIndex > path.get(m).laneIndex) {
          // message = Switch.right();
          // }
          if (message != null) {
            sw.put(Integer.valueOf(path.get(i).pieceIndex), message);
          }
        }
        if (distance < minDistance || (distance == minDistance && sw.size() < switches.size())) {
          LOG.debug(String.format("Shortest path found; distance = %.02f, path = %s, switches = %s", distance, path, sw));
          minDistance = distance;
          switches = sw;
        }
      }

      System.out.println(switches);
    }

    final double distance;
    final double backDistance;
    final boolean skip = false;
    if (previousData == null) {
      distance = 0.0;
      backDistance = 0.0;
    }
    else {

      if (data.piecePosition.pieceIndex.intValue() == previousData.piecePosition.pieceIndex.intValue()) {
        distance = data.piecePosition.inPieceDistance.doubleValue() - previousData.piecePosition.inPieceDistance.doubleValue();
      }
      else {
        final Lane laneA = track.lanes()[previousData.piecePosition.lane.startLaneIndex.intValue()];
        final Lane laneB = track.lanes()[previousData.piecePosition.lane.endLaneIndex.intValue()];
        final double total = track.pieces.get(previousData.piecePosition.pieceIndex.intValue()).curve(Position.zero(), laneA, laneB).length();
        distance = total - previousData.piecePosition.inPieceDistance.doubleValue() + data.piecePosition.inPieceDistance.doubleValue();
        // LOG.debug("Piece jump; total = " + total + " - " + previousData.piecePosition.inPieceDistance.doubleValue());
      }

      backDistance = distance + ((previousData.angle.doubleValue() - data.angle.doubleValue()) / 180 * Math.PI)
          * (cars.get(me).dimensions.length.doubleValue() - cars.get(me).dimensions.guideFlagPosition.doubleValue());

    }

    totalDistance += distance;

    // final double throttle;
    final double angle = data.angle.doubleValue();
    final double radius;
    final Piece piece = track.pieces.get(data.piecePosition.pieceIndex.intValue());

    // if (angle > 0.0) {
    // LOG.warn("Drifting detected! " + angle);
    // }

    if (piece.isStraight()) {
      if (!straight) {
        LOG.debug("Switched to straight");
      }
      straight = true;
    }
    else if (piece.isBend()) {
      if (straight) {
        LOG.debug("Switched to bend");
        final double triggerDiff = Math.abs(distance - maxThrottle * 10);
        if (bends < 0 && triggerDiff < 0.0005 && manyStraights(data.piecePosition.pieceIndex.intValue())) {
          System.out.println("NOW! " + distance);
          bends = 0;
        }
        else {
          LOG.debug("Trigger difference is still " + triggerDiff);
        }
      }
      straight = false;
    }

    final Lane fromLane = track.lanes()[data.piecePosition.lane.startLaneIndex.intValue()];
    final Lane toLane = track.lanes()[data.piecePosition.lane.endLaneIndex.intValue()];

    // factor = 1.0;
    // if (previousData != null && !data.piecePosition.pieceIndex.equals(previousData.piecePosition.pieceIndex)) {
    // if (bends >= 0) {
    // bends++;
    // }
    // }
    // if (bends >= 0 && bends <= 5) {
    // // LOG.debug("Tracking bend pieces");
    // if (bends == 1000000000) {
    // final double pieceDist = data.piecePosition.inPieceDistance.doubleValue();
    // final double len = piece.curve(Position.zero(), fromLane, toLane).length();
    // if (pieceDist >= (0.0 * len)) {
    // LOG.debug("Partial piece");
    // factor = 0.0;
    // }
    // else {
    // LOG.debug("First piece: " + pieceDist + "/" + len);
    // factor = 1.0;
    // }
    // }
    // else if (bends > 4 && bends < 5) {
    // LOG.debug("Full pieces");
    // factor = 0.0;
    // }
    // else {
    // LOG.debug("Disabled");
    // factor = 1.0;
    // }
    // }

    if (piece.isSwitch()) {
      // System.out.println(data.piecePosition.inPieceDistance + " / " + piece.curve(Position.zero(), fromLane,
      // toLane).length());
    }

    if (piece.isStraight()) {
      radius = 0.0;
    }
    else {
      radius = Math.signum(piece.angle.doubleValue()) * piece.radius.doubleValue();
    }

    previousData = new CarData(data);

    final Switch msg = switches != null ? switches.remove(data.piecePosition.pieceIndex) : null;
    if (msg != null) {
      LOG.debug("At " + data.piecePosition.pieceIndex + ", applying " + msg);
      send(msg);
    }
    // else if (turboAvailable && piece.isStraight()) {
    // send(Turbo.newInstance());
    // turboAvailable = false;
    // }
    else {

      if (previousData.piecePosition.lane.endLaneIndex.intValue() == previousData.piecePosition.lane.startLaneIndex.intValue()) {
        velocity.observe(gameTick, distance);
      }

      final double throttle = maxThrottle; // velocity.getThrottle(maxThrottle);
      // factor = Math.min(gameTick / 1000.0, 1.0);
      // System.out.println(factor);

      if (previousDistance != null) {
        if (csv != null) {

          if (piece.isBend()) {
            csv.println(Csv.join(piece.radius, piece.angle, piece.angle / 180 * Math.PI, data.piecePosition.inPieceDistance, angle, angle / 180
                * Math.PI, distance, throttle * factor));
          }
          else {
            csv.println(Csv.join(0.0, 0.0, 0.0, data.piecePosition.inPieceDistance, angle, angle / 180 * Math.PI, distance, throttle * factor));
          }

          csv.flush();

        }
      }
      previousDistance = Double.valueOf(distance);
      previousAngle = Double.valueOf(angle);

      // if (distance < 7) {
      // send(throttle(1.0));
      // }
      // else {
      // send(throttle(Math.max(0.7 + 0.2 * factor, 0.7)));
      // }

      // if (piece.isStraight() || distance < 0.05) {
      // send(throttle(maxThrottle));
      // }
      // else {

      // if (speedUp) {
      // factor = 1.0;
      // if (Math.abs(distance - 8.0) < 0.0001) {
      // speedUp = false;
      // }
      // }
      // else {
      // factor = 0.0;
      // }

      send(throttle(throttle * factor));
      // }
    }

    if (debug && debugWindow != null) {
      debugWindow.update(cars.values());
    }
  }

  private boolean manyStraights(int i) {
    int count = 0;
    while (true) {
      final Piece piece = track.pieces.get(i);
      if (piece.isStraight()) {
        count++;
      }
      else if (piece.isBend()) {
        if (count > 0) {
          return count > 2;
        }
      }
      i = (i + 1) % track.pieces.size();
    }
  }

  private boolean turboAvailable = false;

  private boolean crashed;

  @Override
  protected void onTurboAvailable(final TurboData turboAvailable) throws Exception {
    this.turboAvailable = true;
  }

  @Override
  public void onCrash(final Crash crash) {
    crashed = true;
    cars.get(crash.data).crash();
    if (debug && debugWindow != null) {
      debugWindow.update();
    }
  }

  @Override
  public void onSpawn(final Spawn spawn) {
    cars.get(spawn.data).spawn();
    if (debug && debugWindow != null) {
      debugWindow.update();
    }
    crashed = false;
  }

  @Override
  public void onLapFinished(final LapFinished lapFinished) {
    LOG.info(String.format("Lap finished; totalDistance = %.02f", totalDistance));
    switches = null;
    totalDistance = 0.0;
  }

  @Override
  protected void onFinish(final CarId carId) {
  }

  @Override
  public void onGameEnd(final GameEnd gameEnd) {
    track = null;
    cars = null;
    switches = null;
  }

  @Override
  public void onTournamentEnd(final TournamentEnd tournamentEnd) {
  }

  @Override
  protected void onDisconnect() {
    if (debug && debugWindow != null) {
      debugWindow.dispose();
      debugWindow = null;
    }
  }

}
