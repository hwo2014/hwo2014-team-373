package de.elchundbiber.hwo.game;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import de.elchundbiber.hwo.bot.SlipObserver;
import de.elchundbiber.hwo.bot.VelocityControl;
import de.elchundbiber.hwo.debug.DebugWindow;
import de.elchundbiber.hwo.geometry.Position;
import de.elchundbiber.hwo.graph.Dijkstra;
import de.elchundbiber.hwo.graph.Edge;
import de.elchundbiber.hwo.graph.Vertex;
import de.elchundbiber.hwo.model.BotId;
import de.elchundbiber.hwo.model.Car;
import de.elchundbiber.hwo.model.CarId;
import de.elchundbiber.hwo.model.CarPosition;
import de.elchundbiber.hwo.model.Crash;
import de.elchundbiber.hwo.model.GameEnd;
import de.elchundbiber.hwo.model.GameStart;
import de.elchundbiber.hwo.model.Lane;
import de.elchundbiber.hwo.model.LapFinished;
import de.elchundbiber.hwo.model.Message;
import de.elchundbiber.hwo.model.Piece;
import de.elchundbiber.hwo.model.Race;
import de.elchundbiber.hwo.model.Spawn;
import de.elchundbiber.hwo.model.Switch;
import de.elchundbiber.hwo.model.TournamentEnd;
import de.elchundbiber.hwo.model.Track;
import de.elchundbiber.hwo.model.TurboData;

public class WinningGame extends AbstractGame {

  private static final Logger LOG = Logger.getLogger(WinningGame.class);

  private final Message joinMessage;
  private final boolean debug;
  private final double vMax;

  private DebugWindow debugWindow = null;

  private Track track = null;
  private Lane[] lanes = null;

  private Map<CarId, CarData> cars = null;
  private CarId me = null;
  private PrintWriter csv = null;

  private final SlipObserver slipObserver = new SlipObserver();
  private final VelocityControl velocity = new VelocityControl();

  public WinningGame(final TransportFactory transportFactory, final Message joinMessage, final boolean debug, final double maxThrottle) {
    super(transportFactory);
    this.joinMessage = joinMessage;
    this.debug = debug;
    this.vMax = maxThrottle;
  }

  @Override
  public void onConnect() throws Exception {
    if (debug) {
      debugWindow = new DebugWindow();
    }
    send(joinMessage);
  }

  @Override
  protected void onJoinRace(final BotId botId) {
  }

  @Override
  protected void onYourCar(final CarId carId) {
    me = carId;
  }

  @Override
  public void onGameInit(final Race race) throws Exception {
    track = race.track;
    lanes = track.lanes();
    cars = new HashMap<>();
    try {
      csv = new PrintWriter(new FileOutputStream("log.csv"));
    }
    catch (final IOException e) {
    }
    for (final Car car : race.cars) {
      cars.put(car.id, new CarData(car));
    }
    if (debug && debugWindow != null) {
      debugWindow.update(race.track, cars.values());
    }
  }

  @Override
  public void onGameStart(final GameStart gameStart) {
  }

  private CarData previousData = null;

  private Map<Integer, Message> switches = null;

  private TurboData turbo = null;

  @Override
  protected void onTurboAvailable(final TurboData turbo) throws Exception {
    this.turbo = turbo;
  }

  @Override
  public void onCarPositions(final List<CarPosition> carPositions) throws Exception {
    for (final CarPosition data : carPositions) {
      cars.get(data.id).update(data);
    }
  }

  @Override
  public void onCarPositions(final int gameTick, final List<CarPosition> carPositions) throws Exception {

    /*
     * Update all kind of status stuff
     */

    final CarData data = cars.get(me);
    final Piece piece = track.pieces.get(data.piecePosition.pieceIndex.intValue());

    final double slipAngle;
    if (data.angle != null) {
      slipAngle = data.angle.doubleValue();
    }
    else {
      slipAngle = 0.0;
    }

    final double radius;
    if (piece.isStraight()) {
      radius = 0.0;
    }
    else {
      radius = Math.abs(piece.radius.doubleValue());
    }

    final double angle;
    if (piece.isStraight()) {
      angle = 0.0;
    }
    else {
      angle = Math.abs(piece.angle.doubleValue());
    }

    final boolean enteredPiece = previousData == null || data.piecePosition.pieceIndex.intValue() != previousData.piecePosition.pieceIndex.intValue();

    /*
     * Determine switching strategy, if necessary
     */
    if (switches == null) {
      switches = optimizeSwitches(track, data.piecePosition.pieceIndex.intValue(), data.piecePosition.lane.endLaneIndex.intValue());
      if (LOG.isDebugEnabled()) {
        LOG.debug("Switching strategy: " + switches.toString());
      }
    }

    /*
     * Try to calculate a robust distance, keep null if not possible (in switches)
     */
    final Double distance;
    if (previousData == null) {
      distance = null;
    }
    else {
      if (!enteredPiece) {
        distance = Double.valueOf(data.piecePosition.inPieceDistance.doubleValue() - previousData.piecePosition.inPieceDistance.doubleValue());
      }
      else {
        final int previousStartLaneIndex = previousData.piecePosition.lane.startLaneIndex.intValue();
        final int previousEndLaneIndex = previousData.piecePosition.lane.endLaneIndex.intValue();
        final boolean wasSwitching = previousEndLaneIndex != previousStartLaneIndex;
        if (wasSwitching) {
          distance = null;
        }
        else {
          final Lane previourStartLane = lanes[previousStartLaneIndex];
          final Lane previousEndLane = track.lanes()[previousEndLaneIndex];
          final double totalPreviousLength = track.pieces.get(previousData.piecePosition.pieceIndex.intValue())
              .curve(Position.zero(), previourStartLane, previousEndLane).length();
          distance = Double.valueOf(totalPreviousLength - previousData.piecePosition.inPieceDistance.doubleValue()
              + data.piecePosition.inPieceDistance.doubleValue());
        }
      }
    }

    /*
     * Updating slip observer
     */
    if (enteredPiece) {
      slipObserver.enterPiece(track, piece, data.piecePosition);
    }
    if (distance != null) {
      slipObserver.updateCurrentSpeed(gameTick, distance.doubleValue());
    }
    slipObserver.updateSlipAngle(slipAngle);

    /*
     * Determine throttle
     */
    Double throttle = null;
    if (distance != null) {
      if (velocity.observe(gameTick, distance.doubleValue())) {
        throttle = velocity.getThrottle(0.0);
      }
      else {
        throttle = slipObserver.getThrottle(velocity);
      }
    }

    /*
     * Check if turbo could be applied
     */
    if (turbo != null) {
      final double turboDistance = distance.doubleValue() * turbo.turboFactor.doubleValue() * turbo.turboDurationTicks.intValue();
    }

    /*
     * Apply switches, throttle, or ping
     */
    final Message msg = switches != null ? switches.remove(data.piecePosition.pieceIndex) : null;
    if (msg != null) {
      LOG.debug("Switching at " + data.piecePosition.pieceIndex + ", applying " + msg);
      send(msg);
    }
    else if (throttle != null) {
      send(throttle(throttle.doubleValue()));
    }
    else {
      LOG.warn("Neither switch nor throttle, what am I doing?");
      send(ping());
    }

    /*
     * Debug logging
     */
    if (csv != null && distance != null) {
      csv.println(Csv.join(gameTick, radius, angle, angle / 180 * Math.PI, data.piecePosition.inPieceDistance, slipAngle, slipAngle / 180 * Math.PI,
          distance.doubleValue(), throttle));
      csv.flush();
    }

    previousData = new CarData(data);

    if (debug && debugWindow != null) {
      debugWindow.update(cars.values());
    }

  }

  private Map<Integer, Message> optimizeSwitches(final Track track, final int current, final int currentLane) {

    final Lane[] lanes = track.lanes();
    final List<Edge> edges = new ArrayList<>();
    final int pieces = track.pieces.size();

    final int start = current;
    final int end = (current + pieces - 1) % pieces;

    for (int c = 0; c < track.pieces.size(); ++c) {

      final int i = (start + c) % pieces;
      final int k = (i + 1) % pieces;

      final Piece piece = track.pieces.get(i);

      for (int j = 0; j < lanes.length; ++j) {

        final Vertex from = new Vertex(i, j);
        {
          final Vertex to = new Vertex(k, j);
          final Edge edge = new Edge(from, to, piece.curve(Position.zero(), lanes[j], lanes[j]).length(), null);
          edges.add(edge);
        }

        if (piece.isSwitch()) {
          if (j - 1 >= 0) {
            final Vertex to = new Vertex(k, j - 1);
            final Edge edge = new Edge(from, to, piece.curve(Position.zero(), lanes[j], lanes[j - 1]).length(), Switch.left());
            edges.add(edge);
          }
          if (j + 1 < lanes.length) {
            final Vertex to = new Vertex(k, j + 1);
            final Edge edge = new Edge(from, to, piece.curve(Position.zero(), lanes[j], lanes[j + 1]).length(), Switch.right());
            edges.add(edge);
          }
        }

      }
    }

    LOG.debug(String.format("Graph; edges = %s", edges));

    Map<Integer, Message> switches = Collections.emptyMap();

    double minDistance = Double.POSITIVE_INFINITY;
    for (int j = 0; j < lanes.length; ++j) {
      final Dijkstra dijkstra = new Dijkstra(edges);
      dijkstra.execute(new Vertex(current, currentLane));
      final List<Vertex> path = dijkstra.getPath(new Vertex(end, j));
      final Map<Integer, Message> sw = new TreeMap<>();
      double distance = 0.0;
      for (int i = 0; i < path.size() - 2; ++i) {
        final Vertex a = path.get(i + 1);
        final Vertex b = path.get(i + 2);
        final Edge edge = dijkstra.getDistance(a, b);
        distance += edge.getDistance();
        final Switch message = edge.getMessage();
        if (message != null) {
          sw.put(Integer.valueOf(path.get(i).pieceIndex), message);
        }
      }
      if (distance < minDistance || (distance == minDistance && sw.size() < switches.size())) {
        LOG.debug(String.format("Shortest path found; distance = %.02f, path = %s, switches = %s", distance, path, sw));
        minDistance = distance;
        switches = sw;
      }
    }

    return switches;

  }

  @Override
  public void onCrash(final Crash crash) {
    cars.get(crash.data).crash();
    if (debug && debugWindow != null) {
      debugWindow.update();
    }
  }

  @Override
  public void onSpawn(final Spawn spawn) {
    cars.get(spawn.data).spawn();
    if (debug && debugWindow != null) {
      debugWindow.update();
    }
  }

  @Override
  public void onLapFinished(final LapFinished lapFinished) {
    switches = null;
  }

  @Override
  protected void onFinish(final CarId carId) {
    LOG.info(String.format("One car has finished the race; carId = %s", carId));
  }

  @Override
  public void onGameEnd(final GameEnd gameEnd) {
    track = null;
    lanes = null;
    cars = null;
  }

  @Override
  public void onTournamentEnd(final TournamentEnd tournamentEnd) {
  }

  @Override
  protected void onDisconnect() {
    if (debug && debugWindow != null) {
      debugWindow.dispose();
      debugWindow = null;
    }
  }

}
